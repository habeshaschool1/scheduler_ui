
function openSystemNav() {
  document.getElementById("systemNav").style.width = "100%";
}

function closeSystemNav() {
  document.getElementById("systemNav").style.width = "0%";
}


function openOrgAdminNav() {
  document.getElementById("orgAdminNav").style.width = "100%";
}

function closeOrgAdminNav() {
  document.getElementById("orgAdminNav").style.width = "0%";
}

function openOrgNoneAdminNav() {
  document.getElementById("orgNoneAdminNav").style.width = "100%";
}

function closeOrgNoneAdminNav() {
  document.getElementById("orgNoneAdminNav").style.width = "0%";
}

function openOrgGuestNav() {
  document.getElementById("orgGuestNav").style.width = "100%";
}

function closeOrgGuestNav() {
  document.getElementById("orgGuestNav").style.width = "0%";
}


function openCustomerNav() {
  document.getElementById("customerNav").style.width = "100%";
}

function closeCustomerNav() {
  document.getElementById("customerNav").style.width = "0%";
}


  // ***************START CANAVAS SCRIPT **************
  var canvas = document.getElementById("canvas");
  var ctx = canvas.getContext("2d");
  var radius = canvas.height / 2;
  ctx.translate(radius/2, radius/2);
  radius = radius * 0.60
  setInterval(drawClock, 1000);

  function drawClock() {
    drawFace(ctx, radius);
    drawNumbers(ctx, radius);
    drawTime(ctx, radius);
  }

  function drawFace(ctx, radius) {
    var grad;
    ctx.beginPath();
    ctx.arc(0, 0, radius, 0, 2*Math.PI);
    ctx.fillStyle = '#d9edf7';
    ctx.fill();
    grad = ctx.createRadialGradient(0,0,radius*0.95, 0,0,radius*1.05);
    grad.addColorStop(0, '#247ca8');
    grad.addColorStop(0.5, 'Orange');
    grad.addColorStop(1, '#333');
    ctx.strokeStyle = grad;
    ctx.lineWidth = radius*0.14;
    ctx.stroke();
    ctx.beginPath();
    ctx.arc(0, 0, radius*0.1, 0, 2*Math.PI);
    ctx.fillStyle = 'black';
    ctx.fill();
  }

  function drawNumbers(ctx, radius) {
    var ang;
    var num;
    ctx.font = radius*0.15 + "px arial";
    ctx.textBaseline="middle";
    ctx.textAlign="center";
    for(num = 1; num < 13; num++){
      ang = num * Math.PI / 6;
      ctx.rotate(ang);
      ctx.translate(0, -radius*0.85);
      ctx.rotate(-ang);
      ctx.fillText(num.toString(), 0, 0);
      ctx.rotate(ang);
      ctx.translate(0, radius*0.85);
      ctx.rotate(-ang);
    }
  }

  function drawTime(ctx, radius){
      var now = new Date();
      var hour = now.getHours();
      var minute = now.getMinutes();
      var second = now.getSeconds();
      //hour
      hour=hour%12;
      hour=(hour*Math.PI/6)+
      (minute*Math.PI/(6*60))+
      (second*Math.PI/(360*60));
      drawHand(ctx, hour, radius*0.5, radius*0.07);
      //minute
      minute=(minute*Math.PI/30)+(second*Math.PI/(30*60));
      drawHand(ctx, minute, radius*0.8, radius*0.07);
      // second
      second=(second*Math.PI/30);
      drawHand(ctx, second, radius*0.9, radius*0.02);
  }

  function drawHand(ctx, pos, length, width) {
      ctx.beginPath();
      ctx.lineWidth = width;
      ctx.lineCap = "round";
      ctx.moveTo(0,0);
      ctx.rotate(pos);
      ctx.lineTo(0, -length);
      ctx.stroke();
      ctx.rotate(-pos);
  }

$('#canvas').on("click", function(){
  window.location = '/';
});


$('#language').on("change", function(){
    $.get( "session_language", {language:this.value}, function( data ) {
        location.reload();
    });

});

