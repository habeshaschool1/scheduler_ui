import json
import unicodedata

import requests
from scheduler_ui.helper.constants import api_base_uri


def api_get_calendar(filter={}):
    uri = f"{api_base_uri}/calendar"
    resp = requests.get(uri, params=filter)
    body = unicodedata.normalize('NFKD', resp.text).encode('ascii', 'ignore')
    body = json.loads(body)
    body['status_code'] = resp.status_code
    return body


def api_get_calendar_ethiopia(filter={}):
    uri = f"{api_base_uri}/calendar_ethiopia"
    resp = requests.get(uri, params=filter)
    body = unicodedata.normalize('NFKD', resp.text).encode('ascii', 'ignore')
    body = json.loads(body)
    body['status_code'] = resp.status_code
    return body


def api_post_calendar(bearer=''):
    uri = f"{api_base_uri}/calendar"
    headers = {'Authorization': f"Bearer {bearer}"}
    resp = requests.post(uri, headers=headers)
    body = unicodedata.normalize('NFKD', resp.text).encode('ascii', 'ignore')
    body = json.loads(body)
    body['status_code'] = resp.status_code
    return body


def api_post_calendar_ethiopia(bearer=''):
    uri = f"{api_base_uri}/calendar_ethiopia"
    headers = {'Authorization': f"Bearer {bearer}"}
    resp = requests.post(uri, headers=headers)
    body = unicodedata.normalize('NFKD', resp.text).encode('ascii', 'ignore')
    body = json.loads(body)
    body['status_code'] = resp.status_code
    return body
