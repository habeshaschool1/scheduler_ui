import json
import unicodedata

import requests
from scheduler_ui.helper.constants import api_base_uri


def api_get_organizations(organization_id="", filter={}, bearer=''):
    uri = f"{api_base_uri}/organizations/{organization_id}"
    headers = {'Authorization': f"Bearer {bearer}"}
    resp = requests.get(uri, params=filter, headers=headers)
    body = unicodedata.normalize('NFKD', resp.text).encode('ascii', 'ignore')
    body = json.loads(body)
    body['status_code'] = resp.status_code
    return body


def api_get_organizations_group():
    uri = f"{api_base_uri}/organizations/group"
    resp = requests.get(uri)
    body = unicodedata.normalize('NFKD', resp.text).encode('ascii', 'ignore')
    body = json.loads(body)
    body['status_code'] = resp.status_code
    return body


def api_post_organization(data, bearer=''):
    uri = f"{api_base_uri}/organizations"
    headers = {'Authorization': f"Bearer {bearer}"}
    resp = requests.post(uri, json=data, headers=headers)
    body = unicodedata.normalize('NFKD', resp.text).encode('ascii', 'ignore')
    body = json.loads(body)
    body['status_code'] = resp.status_code
    return body


def api_put_organization(organization_id, data, bearer=''):
    uri = f"{api_base_uri}/organizations/{organization_id}"
    headers = {'Authorization': f"Bearer {bearer}"}
    resp = requests.put(uri, json=data, headers=headers)
    body = unicodedata.normalize('NFKD', resp.text).encode('ascii', 'ignore')
    body = json.loads(body)
    body['status_code'] = resp.status_code
    return body


def api_delete_organization(organization_id, bearer=''):
    uri = f"{api_base_uri}/organizations/{organization_id}"
    headers = {'Authorization': f"Bearer {bearer}"}
    resp = requests.delete(uri, headers=headers)
    body = unicodedata.normalize('NFKD', resp.text).encode('ascii', 'ignore')
    body ={'msg':"organization has been deleted successfully"} # json.loads(body)
    body['status_code'] = resp.status_code
    return body