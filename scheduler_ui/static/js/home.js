
$("input[name=search_by]").on('change', function() {
        v = this.value
        if (v == 'category_id') {
            populate_category()
        }else {
            populate_district()
        }
        $("#search1").val("")
        $("#organization_search").val("").hide("slow")

    });

function populate_district(){
    datum = $.parseJSON($("#districts-data").text())
    search1_options = "<option value=-1 selected disabled>---select city --</option>"
     $.each(datum, function(key, data){
              district_id = data['district_id']
              city = data['city']
              option = "<option value="+ district_id +">"+ city + "</option>"
              search1_options = search1_options + option
        });
     $("#search1").hide("slow").html(search1_options).show("slow")
}



function populate_category(){
    datum = $.parseJSON($("#categories-data").text())
    search1_options = "<option value=-1 selected disabled>---select organization type --</option>"
     $.each(datum, function(key, data){
              category_id = data['category_id']
              name = data['name']
              option = "<option value="+ category_id +">"+ name + "</option>"
              search1_options = search1_options + option
        });
     $("#search1").hide("slow").html(search1_options).show("slow")

}

$('#search1').on('change', function() {
    val = this.value;
    if(val == -1){
        $("#organization_search").hide("slow")
        return false
    }
    search_by = $('input[name="search_by"]:checked').val();
    filter_data = {}
    filter_data[search_by] = val
    populate_organizations(filter_data)
    $("#organization_search").val("").hide("slow").show("slow")

});


function populate_organizations(filter_data){
    // get this service appointment from backend
    $.get( "/organizations_get", filter_data, function( data ) {

    var datum = data['result'] // go get organizations of distric and city
     $('#organizations-data').html(JSON.stringify(datum) )

    organization_datalist_options = ""
     $.each(datum, function(key, data){
              organization_id = data['organization_id']
              name = data['name']
              option = "<option value='"+ organization_id + "-"+name+"'>" + name + "</option>"
              organization_datalist_options = organization_datalist_options + option
        });
     $("#organization_datalist").html(organization_datalist_options)
        $("#organization_search").val("").show("slow").focus()

    })

}


$('#organization_search').on('input', function() {
    var val = this.value;
    // don't take any action unless the input box value is equal to one from our organization list
    if(!$('#organization_datalist option').filter(function(){
        return this.value.toUpperCase() === val.toUpperCase();
    }).length) {
        $('#organization_search').css("border-color", "red")
        $("#organization_info").hide('slow')
        return false
    }
   $('#organization_search').css("border-color", "green")

    var organization_id = val.split("-")[0];
    var data = $.parseJSON($("#organizations-data").text())
    data = find_object_from_array(data, 'organization_id', organization_id)
    populate_organization_info(data)
    populate_services(organization_id)
})


$("#organization_search").on('hide', function(e, visibility){
        $("#organization_info").hide('slow')
})


function populate_organization_info(data){
    $('input[name="organization_id"]').val(data['organization_id'])
    $('input[name="organization_name"]').val(data['name'])
    $('input[name="organization_address"]').val(data['address'])
    $('input[name="organization_city"]').val(data['city'])
    $('input[name="organization_phone"]').val(data['phone'])
    $('input[name="organization_email"]').val(data['email'])

    // update filter area
    category_html = "<span class='fadeout'>" + data['category_name'] + "</span>"
    $("#filter_categories").html(category_html)
    city_html = "<span class='fadeout'>" + data['city'] + "</span>"
    $("#filter_cities").html(city_html)
    organization_html = "<span class='fadeout'>" + data['name'] + "</span>"
    $("#filter_organizations").html(organization_html)


    address =data['name'] +"<br>" + data['address'] + "<br>" + data['phone'] + ", " + data['email'] + "<br>"+data['desc']
    $('#address').html(address)
    $("#organization_info").show('slow')

}


function populate_services(organization_id){
    services = []
    $.get( "/services_get/"+organization_id,{}, function( data ) {
        services = data['result']
        $('#services-data').html( JSON.stringify(services));

        $( "#service_list" ).html("")
        service_options = ""
        $.each(services, function(key, service){
              service_options = service_options +  "<option value="+ service['service_id'] + ">" + service['name'] + "</option>"
        });
        $( "#service_list" ).html(service_options)

    });

    $("#organization_info").show("slow")
    return services
}

$("#time_list").on('change', function(){
    calendar_mode = $("[name=calendar_mode]:checked").val();
    year = $('#day_list').find('option:selected').attr('year');
    month_name = $('#month_list').val();
    month_day = $('#day_list').val();
    year_day = $('#day_list').find('option:selected').attr('year_day');
    weekday_name = $('#day_list').find('option:selected').attr('weekday_name');
    time = $('#time_list').find('option:selected').attr('time');
    complete_date =" (Eastern time: " +  weekday_name + ", " + month_name +" " + month_day + ", "  + year + " at " + time + ")"
    if(calendar_mode=='ethiopian'){
        complete_date =" (በኢትዮጵያ አቆጣጠር: " +  weekday_name + ", " + month_name +" " + month_day + ", "  + year + " በስዓት " + time + ")"
    }

    $("[name=complete_date]").val(complete_date).show("slow")
    $("[name=year_day]").val(year_day)
    $("#complete_date").html(complete_date)
});


$("form").submit(function(){
    phone = $("#phone").val()
    confirm_phone = $("#confirm_phone").val()
    if(!confirm("Is your phone correct?\n"+ phone + "\nif not correct click 'Cancel' and insert again")){
        return false
    }
});

$( document ).ready(function() {
    $("[for=customer_info]").hide()
    $("[for=time_list]").hide()
    $("[for=day_list]").hide()
    $("[for=month_list]").hide()
    $("#calendar_mode").hide()
    $("[for=service_info]").hide()
    $("[for=service_list]").hide()
    $("#organization_info").hide()
    $("#organization_search").hide()
    populate_district()
});
