import json
import unicodedata

import requests
from scheduler_ui.helper.constants import api_base_uri


def api_get_categories(category_id=None, filter={}, bearer=''):
    uri = f"{api_base_uri}/categories/"
    headers = {'Authorization': f"Bearer {bearer}"}
    if category_id:
        filter['category_id'] = int(category_id)

    resp = requests.get(uri, params=filter, headers=headers)
    body = unicodedata.normalize('NFKD', resp.text).encode('ascii', 'ignore')
    body = json.loads(body)
    body['status_code'] = resp.status_code
    return body


def api_post_categories(data, bearer=''):
    uri = f"{api_base_uri}/categories"
    headers = {'Authorization': f"Bearer {bearer}"}
    resp = requests.post(uri, json=data, headers=headers)
    body = unicodedata.normalize('NFKD', resp.text).encode('ascii', 'ignore')
    body = json.loads(body)
    body['status_code'] = resp.status_code
    return body


def api_put_categories(category_id, data, bearer=''):
    uri = f"{api_base_uri}/categories/{category_id}"
    headers = {'Authorization': f"Bearer {bearer}"}
    resp = requests.put(uri, json=data, headers=headers)
    body = unicodedata.normalize('NFKD', resp.text).encode('ascii', 'ignore')
    body = json.loads(body)
    body['status_code'] = resp.status_code
    return body


def api_delete_categories(category_id, bearer=''):
    uri = f"{api_base_uri}/categories/{category_id}"
    headers = {'Authorization': f"Bearer {bearer}"}
    resp = requests.delete(uri, headers=headers)
    # body = unicodedata.normalize('NFKD', resp.text).encode('ascii', 'ignore')
    body = {}  # json.loads(body)
    body['status_code'] = resp.status_code
    return body