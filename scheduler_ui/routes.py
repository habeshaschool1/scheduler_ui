import json

from flask import current_app as app, make_response, session
from flask import url_for, render_template, redirect, request
from flask_babel import Babel, gettext

from scheduler_ui.helper.h_confirm import api_post_confirm_through_email, api_get_confirm
from scheduler_ui.helper.h_email import api_post_email
from scheduler_ui.helper.h_servoice import api_get_services, api_put_service, api_get_service, api_post_services, \
    api_delete_service
from scheduler_ui.helper.h_users import api_put_users, api_post_employee, api_logout, api_login, \
    api_get_users_public_info, api_delete_employees
from scheduler_ui.helper.common import get_list, flash_alert, flash_success, make_google_event_link
from scheduler_ui.helper.constants import MONTHS, WEEKDAYS, USER_SYSTEM, USER_ADMIN, LOCAL_TIME, LOCAL_MONTH_DAYS_DIFF, \
    MONTHS_LOCAL
from scheduler_ui.helper.h_appointment import api_get_appointments, api_put_appointment, \
    api_get_appointments_status, api_put_appointments_status, api_appointments_status_email
from scheduler_ui.helper.h_calendar import api_get_calendar, api_post_calendar, api_get_calendar_ethiopia
from scheduler_ui.helper.h_comments import api_get_comments, api_post_comments, api_delete_comments
from scheduler_ui.helper.h_district import api_get_districts, api_post_districts, api_put_districts, \
    api_delete_districts
from scheduler_ui.helper.h_organization import api_get_organizations, api_put_organization, \
    api_post_organization, api_delete_organization
from scheduler_ui.helper.h_categories import api_get_categories, api_put_categories, api_post_categories, \
    api_delete_categories
import datetime

#    {% if request.cookies.get('Bearer', '') == '' %}


def rend_page(page='home', success_msg=None, alert_msg=None, bearer=None, email=None, organization_id=None,
              access=None):
    if success_msg is not None:
        flash_success(success_msg)
    if alert_msg is not None:
        flash_alert(alert_msg)

    render = make_response(redirect(url_for(page)))
    if bearer is not None:
        render.set_cookie('Bearer', bearer)

    if email is not None:
        render.set_cookie('Email', email)

    if organization_id is not None:
        render.set_cookie('Organization_id', str(organization_id))

    if access is not None:
        render.set_cookie('Access', access)

    return render


"""
pybabel extract -F babel.cfg -o messages.pot --input-dirs=.
pybabel init -i messages.pot -d translations -l am
pybabel update -i messages.pot -d translations -l am
pybabel compile -d translations
"""
babel = Babel(app)


@babel.localeselector
def get_locale():
    return session.get('lang', 'en')
    # return request.accept_languages.best_match(app.config['LANGUAGES'].keys())


@app.route('/session_language')
def session_language():
    language = request.args.get('language').replace(' ', '')
    if language in app.config['LANGUAGES'].keys():
        session['lang'] = language

    return {}


# Home page
@app.route('/')
def home():
    try:
        body_district = api_get_districts()['result']
        body_categories = api_get_categories()['result']
        body_districts_translated = []
        body_categories_translated = []
        for district in body_district:
            city = district['city']
            district['city'] = gettext(city)
            body_districts_translated.append(district)

        for category in body_categories:
            name = category['name']
            category['name'] = gettext(name)
            body_categories_translated.append(category)
    except:
        pass

    today = datetime.datetime.now()
    today_day_of_year = today.timetuple().tm_yday
    return render_template('home.html',
                           body_categories=body_categories_translated,
                           body_districts=body_districts_translated,
                           months=MONTHS,
                           local_time=LOCAL_TIME,
                           today_year_day = today_day_of_year
                           )


# route to "register" page
@app.route('/employees_new', methods=['GET', 'POST'])
def register():
    bearer = request.cookies.get('Bearer', '')
    access = request.cookies.get('Access', '')
    if bearer == '' or access not in [USER_SYSTEM, USER_ADMIN]:
        return rend_page(page='login', alert_msg="You must login as 'SYSTEM' user to create new district")

    msg_txt = "Something wrong to register"
    if request.method == 'POST':
        data = request.form.to_dict()

        data = {"email": data['email'],
                "password": data['password'],
                }
        resp = api_post_employee(data, bearer=bearer)
        msg_txt = resp.get("msg", msg_txt)

        if resp['status_code'] < 400:
            # user_id = resp["result"]["user_id"]
            flash_success(f"Employee '{data['email']}' has been added successfully")
            return rend_page('organizations_update')
        else:
            flash_alert(msg_txt)
    return render_template('/employees_new_page.html')


# route to "register" page
@app.route('/employees_delete', methods=['GET', 'POST'])
def employees_delete():
    bearer = request.cookies.get('Bearer', '')
    access = request.cookies.get('Access', '')
    data = request.form.to_dict()
    user_id = data['user_id']
    if bearer == '' or access not in [USER_SYSTEM, USER_ADMIN]:
        return rend_page(page='login', alert_msg="You must login as 'SYSTEM' user to create new district")

    if request.method == 'POST':
        resp = api_delete_employees(user_id, bearer=bearer)
        if resp['status_code'] < 400:
            flash_success(resp['msg'])
        else:
            flash_alert(resp['msg'])
    return render_template('/employees_new_page.html')


@app.route('/login', methods=['GET', 'POST'])
def login():
    if request.method == 'POST':
        data = request.form.to_dict()
        resp = api_login(data)
        msg_txt = resp.get('msg', "something wrong...")
        if resp['status_code'] <= 400:
            bearer = resp['result']['access_token']
            email = resp['result']['email']
            organization_id = resp['result']['organization_id']
            access = resp['result']['access']
            return rend_page(page='organizations_appointment_page', success_msg=msg_txt, bearer=bearer, email=email,
                             organization_id=organization_id, access=access)

        flash_alert(msg_txt)
    return render_template('/login.html')


@app.route('/logout', methods=['GET', 'POST'])
def logout():
    bearer = request.cookies.get('Bearer', '')
    if bearer == '':
        msg_txt = "You already are signed out!"
        return rend_page(page='login', success_msg=msg_txt, bearer='', email='', organization_id='', access='')

    resp = api_logout(bearer)
    msg_txt = resp.get("msg", "Signed out Successfully!")
    return rend_page(page='login', success_msg=msg_txt, bearer='', email='', organization_id='', access='')


@app.route('/calendar_get', methods=['GET'])
def calendar_get():
    data = request.args.to_dict()
    resp = api_get_calendar(filter=data)
    return json.dumps(resp['result'])


@app.route('/calendar_get_ethiopia', methods=['GET'])
def calendar_get_ethiopia():
    data = request.args.to_dict()
    resp = api_get_calendar_ethiopia(filter=data)
    return json.dumps(resp['result'])


@app.route('/calendar_post', methods=['POST'])
def calendar_post():
    bearer = request.cookies.get('Bearer', '')
    access = request.cookies.get('Access', '')
    if bearer == '' or USER_SYSTEM not in access:
        return rend_page(page='login', alert_msg="You must login as 'SYSTEM' user to update calendar")

    body = api_post_calendar(bearer=bearer)
    return rend_page(page='settings_page', success_msg="Calendar has been updated successfully")


@app.route('/calendar', methods=['GET'])
def calendar_page():
    bearer = request.cookies.get('Bearer', '')
    access = request.cookies.get('Access', '')
    if bearer == '' or USER_SYSTEM not in access:
        return rend_page(page='login', alert_msg="You must login as 'SYSTEM' user to update calendar")

    body_calendar = api_get_calendar()['result']
    return render_template('settings_page.html',
                           weekdays=WEEKDAYS,
                           months=MONTHS,
                           local_month_day_diff=LOCAL_MONTH_DAYS_DIFF,
                           datas=body_calendar)


@app.route('/districts_get', methods=['GET'])
def districts_get():
    data = request.args.to_dict()
    resp = api_get_districts(filter=data)
    return resp


@app.route('/districts_post', methods=['POST'])
def districts_post():
    data = request.form.to_dict()
    resp = api_post_districts(data)
    try:  # give only the first record if we have multiple
        resp['result'] = resp['result'][0]
    except:
        pass

    return resp


@app.route('/districts_update/<district_id>', methods=['GET', 'POST'])
def districts_put(district_id):
    bearer = request.cookies.get('Bearer', '')
    access = request.cookies.get('Access', '')
    if bearer == '' or USER_SYSTEM not in access:
        return rend_page(page='login', alert_msg="You are not authorized to do this action")

    if request.method == 'GET':
        district_data = api_get_districts(district_id=district_id, bearer=bearer)['result'][0]
        return render_template('districts_update_page.html', data=district_data)
    else:
        data = request.form.to_dict()
        resp = api_put_districts(int(district_id), data, bearer=bearer)

        if resp['status_code'] < 400:
            flash_success(resp['msg'])
        else:
            flash_alert(resp['msg'])
        return redirect(url_for('districts'))


@app.route('/districts_delete/<district_id>', methods=['POST'])
def districts_delete(district_id):
    bearer = request.cookies.get('Bearer', '')
    access = request.cookies.get('Access', '')
    if bearer == '' or USER_SYSTEM not in access:
        return rend_page(page='login', alert_msg="You are not authorized to do this action")

    resp = api_delete_districts(int(district_id), bearer=bearer)
    if resp['status_code'] < 400:
        flash_success(f"deleted successfully, district {district_id}")
    else:
        flash_alert(f"not deleted district {district_id}")
    return {}


@app.route('/districts', methods=['GET'])
def districts():
    data = request.args.to_dict()
    districts_data = api_get_districts(filter=data)
    return render_template('districts_page.html', datas=districts_data)


@app.route('/districts_new', methods=['GET', 'POST'])
def districts_new():
    bearer = request.cookies.get('Bearer', '')
    access = request.cookies.get('Access', '')
    if bearer == '' or USER_SYSTEM not in access:
        return rend_page(page='login', alert_msg="You are not authorized to do this action")
    if request.method == 'GET':
        return render_template('districts_new_page.html')

    data = request.form.to_dict()
    body = api_post_districts(data, bearer=bearer)
    if body['status_code'] < 400:
        return rend_page('districts', success_msg=body['msg'])
    else:
        return rend_page('districts', alert_msg=body['msg'])


@app.route('/categories_get', methods=['GET'])
def categories_get():
    data = request.args.to_dict()
    data = api_get_categories(filter=data)
    return data


@app.route('/categories_post', methods=['POST'])
def categories_post():
    data = request.form.to_dict()
    resp = api_post_categories(data)
    try:  # give only the first record if we have multiple
        resp['result'] = resp['result'][0]
    except:
        pass
    return resp


@app.route('/categories_update/<category_id>', methods=['GET', 'POST'])
def categories_put(category_id):
    bearer = request.cookies.get('Bearer', '')
    access = request.cookies.get('Access', '')
    if bearer == '' or USER_SYSTEM not in access:
        return rend_page(page='login', alert_msg="You are not authorized to do this action")

    if request.method == 'GET':
        category_data = api_get_categories(category_id=category_id)['result'][0]
        return render_template('categories_update_page.html', data=category_data)
    else:
        data = request.form.to_dict()
        resp = api_put_categories(int(category_id), data, bearer=bearer)

        if resp['status_code'] < 400:
            flash_success(resp['msg'])
        else:
            flash_alert(resp['msg'])
        return redirect(url_for('categories'))


@app.route('/categories_delete/<category_id>', methods=['POST'])
def categories_delete(category_id):
    bearer = request.cookies.get('Bearer', '')
    access = request.cookies.get('Access', '')
    if bearer == '' or USER_SYSTEM not in access:
        return rend_page(page='login', alert_msg="You are not authorized to do this action")

    resp = api_delete_categories(int(category_id), bearer=bearer)
    if resp['status_code'] < 400:
        flash_success(f"deleted successfully, category {category_id}")
    else:
        flash_alert(f"not deleted category {category_id}")
    return {}


@app.route('/categories', methods=['GET'])
def categories():
    data = request.args.to_dict()
    categories_data = api_get_categories(filter=data)
    return render_template('categories_page.html', datas=categories_data)


@app.route('/confirm/through_email', methods=['GET', 'POST'])
def confirm():
    if request.method == 'GET':
        data = request.args.to_dict()
        data = {'pincode': data['pincode'], 'email': data['email']}
        body = api_get_confirm(data)
        return body
    else:
        data = request.form.to_dict()
        body = api_post_confirm_through_email(data)
        return body


@app.route('/categories_new', methods=['GET', 'POST'])
def categories_new():
    bearer = request.cookies.get('Bearer', '')
    access = request.cookies.get('Access', '')
    if bearer == '' or USER_SYSTEM not in access:
        return rend_page(page='login', alert_msg="You are not authorized to do this action")

    if request.method == 'GET':
        return render_template('categories_new_page.html')
    else:
        data = request.form.to_dict()
        body = api_post_categories(data, bearer=bearer)
        if body['status_code'] < 400:
            return rend_page('categories', success_msg=body['msg'])
        else:
            return rend_page('categories', alert_msg=body['msg'])


@app.route('/organizations_get/<organization_id>', methods=['GET'])
@app.route('/organizations_get/', methods=['GET'])
def organizations_get(organization_id=''):
    data = request.args.to_dict()
    resp = api_get_organizations(organization_id=organization_id, filter=data)
    return resp


@app.route('/organizations', methods=['GET'])
def organizations():
    data = request.args.to_dict()
    organizations_data = api_get_organizations(filter=data)
    return render_template('organizations_page.html', datas=organizations_data)


@app.route('/organizations_new', methods=['GET', 'POST'])
def organizations_new():
    if request.method == 'GET':
        body_categories = api_get_categories()
        body_districts = api_get_districts()
        if body_categories['status_code'] >= 400:
            flash_alert(body_categories['msg'])
        elif body_districts['status_code'] >= 400:
            flash_alert(body_districts['msg'])

        return render_template('organizations_new_page.html',
                               body_categories=body_categories,
                               body_districts=body_districts
                               )
    else:
        data = request.form.to_dict()
        organization_data = {
            'pincode': data['pincode'],
            'category_id': data['category_id'],
            'district_id': data['district_id'],
            'name': data['name'],
            'address': data['address'],
            'phone': data['phone'],
            'password': data['password']
        }

        body = api_post_organization(organization_data)
        if body['status_code'] < 400:
            # login user
            email = body['result']['email']
            login_data = {'email': email, 'password': data['password']}
            resp = api_login(login_data)
            msg_txt = resp.get('msg', "something wrong...")
            if resp['status_code'] < 400:
                bearer = resp['result']['access_token']
                email = resp['result']['email']
                organization_id = resp['result']['organization_id']
                access = resp['result']['access']
                return rend_page(page='organizations_update', success_msg=msg_txt, bearer=bearer, email=email,
                                 organization_id=organization_id, access=access)

            return rend_page(page='login',
                             success_msg="organization has been created successfully. Now login with your email and password")
        else:
            return render_template('fail.html', msg=body['msg'])


@app.route('/organizations_update/<organization_id>', methods=['GET', 'POST'])
@app.route('/organizations_update', methods=['GET', 'POST'])
def organizations_update(organization_id=None):
    bearer = request.cookies.get('Bearer', '')
    email = request.cookies.get('Email', '')
    access = request.cookies.get('Access', '')
    organization_id = organization_id or request.cookies.get('Organization_id', '')

    if organization_id == '' or bearer == '' or access.lower() not in [USER_ADMIN, USER_SYSTEM]:
        return rend_page(page='login', alert_msg="You are not authorized to do this task")

    if request.method == 'GET':
        body_organizations = api_get_organizations(organization_id=organization_id, bearer=bearer)
        body_categories = api_get_categories()
        body_districts = api_get_districts()
        if body_categories['status_code'] >= 400:
            return rend_page(page='categories', alert_msg=body_categories['msg'])

        if body_districts['status_code'] >= 400:
            return rend_page(page='districts', alert_msg=body_districts['msg'])

        body_users = api_get_users_public_info(filter={'organization_id': organization_id}, bearer=bearer)
        body_services = api_get_services({'organization_id': organization_id}, bearer=bearer)
        services_list = []
        for service in body_services['result']:
            services_list.append({'service_id': service['service_id'], 'name': service['name']})

        return render_template('organizations_update_page.html', body_organization=body_organizations['result'],
                               body_categories=body_categories['result'],
                               body_districts=body_districts['result'],
                               body_users=body_users['result'],
                               body_services=services_list,
                               )

    elif request.method == 'POST':
        data = request.form.to_dict()
        body = api_put_organization(organization_id, data=data, bearer=bearer)
        if body['status_code'] < 400:
            flash_success(body['msg'])
        else:
            flash_alert(body['msg'])

        return rend_page('organizations_update')


@app.route('/organizations_delete/<organization_id>', methods=['GET'])
def organizations_delete(organization_id):
    bearer = request.cookies.get('Bearer', '')
    email = request.cookies.get('Email', '')
    access = request.cookies.get('Access', '').lower()
    organization_id2 = request.cookies.get('Organization_id', '')
    if organization_id2 == '' or bearer == '' or access not in [USER_SYSTEM, USER_ADMIN]:
        return rend_page(page='login', alert_msg="You are not authorized to do this task")

    if organization_id == organization_id2 and access == USER_SYSTEM:
        return rend_page(page='login', alert_msg="System org can't be deleted")

    if organization_id != organization_id2 and access != USER_SYSTEM:
        return rend_page(page='login', alert_msg="You are not allowed to delete someone's organization")

    body = api_delete_organization(organization_id, bearer=bearer)
    if body['status_code'] < 400:
        return rend_page('logout',
                         success_msg="Your org. has been deleted permanently. All users, services and all appointments has been deleted permanently")
    else:
        return rend_page(page='organizations_update', alert_msg="something wrong, org. has not been fully deleted")


@app.route('/services_get/<organization_id>', methods=['GET'])
def services_get(organization_id=''):
    filter_data = request.args.to_dict()
    if organization_id != '':
        filter_data['organization_id'] = organization_id
    resp = api_get_services(filter_data)
    return resp


@app.route('/services_new', methods=['GET', 'POST'])
def services_new():
    if request.method == 'GET':
        body_categories = api_get_categories()
        body_districts = api_get_districts()
        if body_categories['status_code'] >= 400:
            return rend_page(page='categories', alert_msg=body_categories['msg'])

        if body_districts['status_code'] >= 400:
            return rend_page(page='districts', alert_msg=body_districts['msg'])

        return render_template('organizations_new_page.html',
                               body_categories=body_categories,
                               body_districts=body_districts
                               )
    else:
        bearer = request.cookies.get('Bearer', '')
        email = request.cookies.get('Email', '')
        access = request.cookies.get('Access', '')
        organization_id = request.cookies.get('Organization_id', '')
        data = request.form.to_dict()
        name = data['name']
        if data.get('organization_id', "") != "":
            organization_id = data['organization_id']

        if organization_id == '' or access.lower() not in [USER_ADMIN, USER_SYSTEM]:
            return rend_page(page='login', alert_msg="You should login as admin to update organization services")

        post_data = {
            'name': name,
            'organization_id': organization_id
        }
        body = api_post_services(post_data, bearer=bearer)
        return body


@app.route('/services_delete', methods=['POST'])
def services_delete():
    bearer = request.cookies.get('Bearer', '')
    email = request.cookies.get('Email', '')
    access = request.cookies.get('Access', '')
    organization_id = request.cookies.get('Organization_id', '')
    if organization_id == '' or access.lower() not in ['admin', 'system']:
        return rend_page(page='login', alert_msg="You should login to access organization data")

    data = request.form.to_dict()
    service_id = data['service_id']
    body = api_delete_service(service_id, bearer=bearer)
    if body['status_code'] < 400:
        flash_success(f"Service {service_id} has been deleted successfully")
    else:
        flash_alert(
            f"Unable to delete {service_id} service. Try again, or talk to system administrators for special help")
    return body


def exclude_weekdays(data):
    service_exclude_weekdays = []
    for weekday in WEEKDAYS.keys():
        if data.get(weekday, False):
            service_exclude_weekdays.append(weekday)
    return service_exclude_weekdays


def exclude_months(data):
    service_excluded_months = []
    for month in MONTHS.keys():
        if data.get(month, False):
            service_excluded_months.append(month)
    return service_excluded_months


def exclude_months_local(data):
    service_excluded_months = []
    for month in MONTHS_LOCAL.keys():
        if data.get(month, False):
            service_excluded_months.append(month)
    return service_excluded_months


def prepare_capacity(data):
    # prepare capacity data
    capacity = {}
    for key, value in data.items():
        if key.endswith("_am") or key.endswith("_pm"):
            capacity[key] = value

    return capacity


@app.route('/services_update/<service_id>', methods=['GET', 'POST'])
def services_update(service_id):
    bearer = request.cookies.get('Bearer', '')
    email = request.cookies.get('Email', '')
    access = request.cookies.get('Access', '')
    organization_id = request.cookies.get('Organization_id', '')

    if organization_id == '' or access.lower() not in ['admin', 'system']:
        return rend_page(page='login', alert_msg="You should login as admin to update organization services")
    if request.method == 'GET':
        body = api_get_service(service_id, bearer=bearer)
        if body['status_code'] < 400:
            return render_template('/services_update_page.html',
                                   data=body['result'],
                                   weekdays=WEEKDAYS,
                                   months=MONTHS,
                                   local_time=LOCAL_TIME
                                   )
        else:
            rend_page('organizations_update_page', alert_msg=body['msg'])
    elif request.method == 'POST':
        data = request.form.to_dict()
        put_data = {
            "desc": data['desc'],
            "exclude_months": exclude_months(data),
            "exclude_months_local": exclude_months_local(data),
            "exclude_weekdays": exclude_weekdays(data),
            "holidays": get_list(data, 'holidays'),
            "requirements": get_list(data, 'requirements'),
            "name": data['name'],
            'capacity': prepare_capacity(data)
        }

        body = api_put_service(service_id, data=put_data, bearer=bearer)
        if body['status_code'] < 400:
            flash_success(f"Service '{data['name']}' has been added successfully")
            return rend_page('organizations_update')
        else:
            flash_alert(body['msg'])
            put_data['service_id'] = service_id
            return render_template('services_update_page.html', data=put_data)


@app.route('/appointments_get', methods=['GET'])
def appointments_get():
    data = request.args.to_dict()
    resp = api_get_appointments(filter=data)

    return resp['result']


@app.route('/appointments_post', methods=['GET', 'POST'])
def appointments_post():
    data = request.form.to_dict()
    phone = int(data['phone'])
    post_data = {
        "organization_id": data['organization_id'],
        "service_id": data['service_id'],
        "fullname": data['fullname'],
        "phone": phone,
        "m": data['m'],
        "d": data['d'],
        "t": data['t'],
        "year_day": data['year_day'],
        "complete_date": data['complete_date']
    }

    body = api_put_appointment(post_data)
    if body['status_code'] < 400:
        pincode = body['result']['pincode']
        flash_success(
            "Appointment has been booked. Save this confirmation letter for your record.")
        return redirect(url_for('appointments_status_page', phone=phone, pincode=pincode))
    else:
        return render_template('fail.html', msg=body['msg'])


@app.route('/appointments_status_get', methods=['GET', 'POST'])
def appointments_status_get():
    appointments_data = {}
    organization_id = int(request.cookies.get('Organization_id', -1))
    data = request.form.to_dict()
    phone = data.get('phone', "")
    if len(str(phone)) > 0 and organization_id > 0:
        data["organization_id"] = organization_id
        resp = api_get_appointments_status(filter=data)
        return resp
    else:
        data["organization_id"] = organization_id
        data['result'] = []
        return data


@app.route('/appointments_status_email', methods=['POST'])
def appointments_status_email():
    data = request.form.to_dict()
    resp = api_appointments_status_email(filter=data)
    return resp


@app.route('/appointments_status_page', methods=['GET', 'POST'])
def appointments_status_page():
    data = request.values
    phone = data.get('phone', "").strip()
    pincode = data.get('pincode', "").strip()
    if not pincode or not phone:
        return render_template('appointments_status_page.html',
                               organization_data={},
                               service_data={},
                               appointment_data={},
                               phone=phone,
                               pincode=pincode
                               )

    filter_data = {'phone': phone,
                   'pincode': pincode
                   }

    appointment_data = api_get_appointments_status(filter=filter_data)['result']
    if not appointment_data:
        return render_template('appointments_status_page.html',
                               organization_data={},
                               service_data={},
                               appointment_data={},
                               phone=phone,
                               pincode=pincode
                               )
    appointment_data = appointment_data[0]
    d = appointment_data['d']
    t = appointment_data['t']
    hour, minute, am_pm = t.replace('t', '').split('_')
    organization_data = api_get_organizations(organization_id=appointment_data['organization_id'])['result']
    service_data = api_get_services({'service_id': appointment_data['service_id']})['result'][0]
    google_event_link = make_google_event_link('2014', 1, d, hour, minute, appointment_data['phone'], appointment_data['pincode'], service_data['name'], organization_data['name'])

    return render_template('appointments_status_page.html',
                           organization_data=organization_data,
                           service_data=service_data,
                           appointment_data=appointment_data,
                           phone=phone,
                           pincode=pincode,
                           google_event_link=google_event_link
                           )


@app.route('/customers_appointment_page/<service_id>', methods=['GET'])
def customers_appointment_page(service_id=None):
    body_districts = api_get_districts()
    body_categories = api_get_categories()
    body_organizations = api_get_organizations()
    body_service_list = []
    body_selected_service = {"service_id": 0}

    if body_categories['status_code'] >= 400:
        return rend_page(page='categories', alert_msg=body_categories['msg'])

    if body_organizations['status_code'] >= 400:
        return rend_page(page='home', alert_msg=body_organizations['msg'])

    return render_template('customers_appointment_page.html',
                           body_categories=body_categories,
                           body_districts=body_districts,
                           body_organizations=body_organizations,
                           body_service_list=body_service_list,
                           body_selected_service=body_selected_service
                           )


@app.route('/organizations_appointment_page', methods=['GET'])
def organizations_appointment_page():
    bearer = request.cookies.get('Bearer', '')
    email = request.cookies.get('Email', '')
    access = request.cookies.get('Access', '')
    organization_id = request.cookies.get('Organization_id', '')
    if organization_id == '' or access.lower() not in ['admin', 'system', 'user']:
        return rend_page(page='login', alert_msg="You should login to access organization data")

    body_organization = api_get_organizations(organization_id=organization_id)
    if body_organization['status_code'] >= 400:
        return rend_page(page='login', alert_msg=body_organization['msg'])

    body_services = api_get_services({'organization_id': organization_id})
    return render_template('organizations_appointment_page.html',
                           body_organization=body_organization['result'],
                           body_services=body_services['result'],
                           months=MONTHS,
                           local_time=LOCAL_TIME
                           )


@app.route('/organizations_appointment_status', methods=['GET', 'POST'])
def organizations_appointments_status_page():
    data = request.form.to_dict()
    if request.method == 'GET':
        appointments_data = api_get_appointments_status(filter=data)['result'][0]
        return appointments_data
    elif request.method == 'POST':
        email = request.cookies.get('Email', 'Client')
        put_data = {
            "service_id": data['service_id'],
            "m": data['m'],
            "d": data['d'],
            "pincode": data['pincode'].strip(),
            "status": data['status'],
            "reason": f"{data['reason']} --Updated by: {email}"
        }
        appointments_data = api_put_appointments_status(put_data)
        return appointments_data


@app.route('/comments', methods=['GET'])
def comments():
    data = request.args.to_dict()
    comments_data = api_get_comments(filter=data)
    return render_template('comments_page.html', datas=comments_data)


@app.route('/comments_delete/<_id>', methods=['POST'])
def comments_delete(_id):
    bearer = request.cookies.get('Bearer', '')
    access = request.cookies.get('Access', '')
    if bearer == '' or USER_SYSTEM not in access:
        return rend_page(page='login', alert_msg="You are not authorized to do this action")

    resp = api_delete_comments(int(_id), bearer=bearer)
    if resp['status_code'] < 400:
        flash_success(f"deleted successfully")
    else:
        flash_alert(f"not deleted")
    return {}


@app.route('/comments_post', methods=['GET', 'POST'])
def comments_post_page():
    data = request.form.to_dict()
    if request.method == 'GET':
        return render_template('comments_new_page.html')
    elif request.method == 'POST':
        phone = data.get('phone')
        email = data.get('email')
        try:
            phone = int(phone)
        except:
            phone = 251910101010

        if "@" not in email:
            email = "anonimus@ketero.com"

        post_data = {
            "phone": phone,
            "email": email,
            "comment": data['comment']
        }
        body = api_post_comments(post_data)
        if body['status_code'] <= 399:
            return render_template('success_page.html', title="Comment has been posted successfully. Thank you!",
                                   msg=body['msg'])
        else:
            return render_template('fail.html', msg=body['msg'])


@app.route('/success_page')
def success_page():
    return render_template('success_page.html')


@app.route('/under_construction_page')
def under_construction_page():
    return render_template('under_construction_page.html')


@app.context_processor
def inject_template_scope():
    injections = dict()

    def cookies_check():
        value = request.cookies.get('cookie_consent')
        return value == 'true'

    injections.update(cookies_check=cookies_check)

    def about():
        if session.get('lang', 'en') == 'en':
            return """
             We, Damena-Teck, provides IT based solutions for our customers. We always use latest technologies and software development standards to make our solutions more secure and high in performance.
                            Our team is ranged from matured to young, experienced to energetic ones from different citizens.
                            Do you want us to get you any IT support, or want to work with us? Just contact us through our Phone, Email or Contact-us link shown below.
            """
        return """እኛ ዳመና-ቴክ ለደንበኞቻችን በአይቲ ላይ የተመሰረቱ መፍትሄዎችን እንሰጣለን። መፍትሄዎቻችንን የበለጠ አስተማማኝ እና በአፈጻጸም ከፍተኛ ለማድረግ ሁልጊዜ አዳዲስ ቴክኖሎጂዎችን እና የሶፍትዌር ልማት መስፈርቶችን እንጠቀማለን።
                            ቡድናችን ከተለያዩ ዜጎች የመጡ ከጎለመሰ እስከ ወጣት ፣ ከልምድ እስከ ጉልበት ያላቸው ናቸው።
                            ማንኛውንም የአይቲ ድጋፍ እንድናገኝልዎ ይፈልጋሉ ወይም ከእኛ ጋር መስራት ይፈልጋሉ? ከዚህ በታች በሚታየው በስልክ ፣ በኢሜል ወይም በእውቂያ-አገናኝ በኩል እኛን ያነጋግሩን።
        """

    injections.update(about=about)

    return injections


@app.route('/emails_post', methods=['POST'])
def emails_post():
    data = request.form.to_dict()
    resp = api_post_email(data)
    return resp
