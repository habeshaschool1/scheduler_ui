

$('#enable_edit_organization_info').on("click", function(){
    if($("#enable_edit_organization_info").text() =="Cancel changes"){
        location.reload();
    }
    $("#enable_edit_organization_info").text('Cancel changes')
    $("#organization_info_form").toggle('slow')
});

$('#add_new_service').on("click", function(){
    pageURL = $(location).attr("href");
    parts = pageURL.split("/")
    organization_id = parts[parts.length-1];
    organization_id = parseInt(organization_id) || ""

    name = prompt("Type new service name here")
    validate_name = name.replace(/\s+/g, '')
    if(validate_name.length>0 & validate_name !='null'){
         $.post( "/services_new",{name:name, organization_id:organization_id}, function( data ) {
            service_id = data['result']['service_id']
            window.location = '/services_update/' + service_id;

        });
    }
});


$('span[name="delete_service"]').on("click", function(){
    service_id = $(this).attr('service_id')
    name = $(this).attr('service_name')

    confirm_name = prompt("Deleting service\n\"" + name +"\"\n \nRetype service name again")
    if(confirm_name == name){
        $.post( "/services_delete", {service_id: service_id}, function( data ) {
         location.reload()
        });
    }else{
        alert("what you typed is not matching\n"+ name + "\n"+ confirm_name + "\n try again")
        return false
    }

});


$('#add_new_user').on("click", function(){
        window.location = '/employees_new'
});


$('span[name="delete_user"]').on("click", function(){
    user_id = $(this).attr('user_id')
    name = $(this).attr('user_name')

    confirm_name = prompt("Deleting user\n\"" + name +"\"\n  \nRetype email again")
    if(confirm_name == name){
        $.post( "/employees_delete", {user_id: user_id}, function( data ) {
         location.reload()
        });
    }else{
        alert("what you typed is not matching\n"+ name + "\n"+ confirm_name + "\n try again")
        return false
    }

});


$('span[name="delete_organization"]').on("click", function(){
    organization_id = $(this).attr('organization_id')
    name = $(this).attr('organization_name')

    confirm_name = prompt("Deleting organization\n\"" + name +"\"\nThis action will  archive all users ,services and appointments of this org.\n Talk to this site managers, if you want to either delete forever or recover back your org. \nThis is just for your safety\n \nRetype organization name again")
    if(confirm_name == name){
        $.get( "/organizations_delete/" +organization_id, {}, function( data ) {
         location.reload()
        });
    }else{
        alert("what you typed is not matching\n"+ name + "\n"+ confirm_name + "\n try again")
        return false
    }

});

$( document ).ready(function() {
    $("#organization_info_form").hide()

});
