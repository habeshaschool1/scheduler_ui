import json
import unicodedata

import requests
from scheduler_ui.helper.constants import api_base_uri


def api_get_comments(filter={}, bearer=''):
    uri = f"{api_base_uri}/comments"
    headers = {'Authorization': f"Bearer {bearer}"}
    resp = requests.get(uri, params=filter, headers=headers)
    body = unicodedata.normalize('NFKD', resp.text).encode('ascii', 'ignore')
    body = json.loads(body)
    body['status_code'] = resp.status_code
    return body


def api_post_comments(data):
    uri = f"{api_base_uri}/comments"
    resp = requests.post(uri, json=data)
    body = unicodedata.normalize('NFKD', resp.text).encode('ascii', 'ignore')
    body = json.loads(body)
    body['status_code'] = resp.status_code
    return body


def api_delete_comments(_id, bearer=''):
    uri = f"{api_base_uri}/comments/{_id}"
    headers = {'Authorization': f"Bearer {bearer}"}
    resp = requests.delete(uri, headers=headers)
    # body = unicodedata.normalize('NFKD', resp.text).encode('ascii', 'ignore')
    body = {}  # json.loads(body)
    body['status_code'] = resp.status_code
    return body