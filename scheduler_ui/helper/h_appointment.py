import json
import unicodedata

import requests
from scheduler_ui.helper.constants import api_base_uri


def api_get_appointment(service_id):
    uri = f"{api_base_uri}/appointments/{service_id}"
    resp = requests.get(uri)
    body = unicodedata.normalize('NFKD', resp.text).encode('ascii', 'ignore')
    body = json.loads(body)
    body['status_code'] = resp.status_code
    return body


def api_get_appointments(filter={}):
    uri = f"{api_base_uri}/appointments"
    resp = requests.get(uri, params=filter)
    body = unicodedata.normalize('NFKD', resp.text).encode('ascii', 'ignore')
    body = json.loads(body)
    body['status_code'] = resp.status_code
    return body


def api_put_appointment(data):
    uri = f"{api_base_uri}/appointments"
    resp = requests.put(uri, json=data)
    body = unicodedata.normalize('NFKD', resp.text).encode('ascii', 'ignore')
    body = json.loads(body)
    body['status_code'] = resp.status_code
    return body


def api_get_appointments_status(filter={}):
    uri = f"{api_base_uri}/appointments_status"
    resp = requests.get(uri, params=filter)
    body = unicodedata.normalize('NFKD', resp.text).encode('ascii', 'ignore')
    body = json.loads(body)
    body['status_code'] = resp.status_code
    return body


def api_appointments_status_email(filter={}):
    uri = f"{api_base_uri}/appointments_status/email"
    resp = requests.get(uri, params=filter)
    body = unicodedata.normalize('NFKD', resp.text).encode('ascii', 'ignore')
    body = json.loads(body)
    body['status_code'] = resp.status_code
    return body


def api_put_appointments_status(data, bearer=''):
    uri = f"{api_base_uri}/appointments_status"
    headers = {'Authorization': f"Bearer {bearer}"}
    resp = requests.put(uri, json=data, headers=headers)
    body = unicodedata.normalize('NFKD', resp.text).encode('ascii', 'ignore')
    body = json.loads(body)
    body['status_code'] = resp.status_code
    return body
