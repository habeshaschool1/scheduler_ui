
$("#organization_info").on('hide', function(e, visibility){
        $("[for=service_list]").hide('slow')

})

$("#organization_info").on('show', function(e, visibility){
        $("[for=service_list]").show('slow').focus()

})

$("[for=service_list]").on('hide', function(e, visibility){
        $("[for=service_info]").hide('slow')
})


$("[for=service_info]").on('hide', function(e, visibility){
        $("#calendar_mode").hide('slow')
})

$("#calendar_mode").on('hide', function(e, visibility){
        $("[for=month_list]").hide('slow')
})


$("[for=month_list]").on('hide', function(e, visibility){
        $("[for=day_list]").hide('slow')
})


$("[for=day_list]").on('hide', function(e, visibility){
        $("[for=time_list]").hide('slow')
})

$("[for=time_list]").on('hide', function(e, visibility){
        $("[for=customer_info]").hide('slow')
})


$("[for=time_list]").on('change', function(e, visibility){
        $("[for=customer_info]").show('slow').focus()
})

function populate_service_info(data){
    $('#service_desc').html(data['desc'])

    req = ""
    $.each(data['requirements'], function(index, requirement){
        if (requirement.trim()) {
            name = 'service_requirement' + (index+1)
            req = req + "<input type='checkbox' name='"+ name+"' value='"+requirement +"' required>" + requirement + "<br>"
         }
    });

    if(req.length != 0){ req = "<p class='orange'>Read and check all the bellow requirements</p>"+req}
    $('#service_requirements').html(req)
     holidays = data['holidays'].join(", ")
    if(holidays.length != 0){ holidays = "<p class='orange'>Closed Days<br></p>"+holidays}
    $('#service_holidays').html(holidays)

    $("[for=service_info]").show('slow').focus();
}

$("#service_list").on("change", function(){
    $("[for=service_info]").hide('slow')
    service_id = this.value

    services = $.parseJSON($("#services-data").text())
    selected_service = find_object_from_array(services, 'service_id', service_id)
    $('#service-data').html(JSON.stringify(selected_service));
    populate_service_info(selected_service)
    $("[name=calendar_mode]").prop("checked", false)
    $("#calendar_mode").show('slow').focus()

});


$("[name=calendar_mode]").on("change", function(){
    populate_month()
});


$("#month_list").on("change", function(){
    $("[for=day_list]").hide('slow')
    var month_selected = this.value
    var calendar_mode = $("[name=calendar_mode]:checked").val();
    populate_days(month_selected, calendar_mode)
    $("[for=day_list]").show('slow').focus()
});


$("#day_list").on('change', function() {
    $("[for=time_list]").hide('slow')
    year = $('#day_list').find('option:selected').attr('year');
    month_name = $('#month_list').val();
    month_day = $('#day_list').val();
    year_day = $('#day_list').find('option:selected').attr('year_day');

    $.get( "appointments_get", {service_id:service_id, year_day:year_day, m:month_name, d: month_day, year:year }, function( data ) {
        if ( $('#time_list > tbody').length){
         populate_time_organizations(data)
        }else{
         populate_time_customers(data)
        }
    });

});


$("#time_list").on('change', function() {
    $("[for=customer_info]").hide('slow').show('slow').focus()
});

function populate_month(){

    calendar_mode = $("[name=calendar_mode]:checked").val();

    $("#month_list").val(-1)
    $("[for=month_list]").hide('slow')

    $('#appointment-date-data').html('{}');
    // populate month
    service_data = $.parseJSON($("#service-data").text())
    exclude_months = service_data['exclude_months']
    exclude_months_local = service_data['exclude_months_local']
    calendar_get_method = "calendar_get"
    if(calendar_mode.includes("ethiopian")){
        calendar_get_method = "calendar_get_ethiopia"
    }
    // get days of given month
    $.get( calendar_get_method, function(data) {
        month_options = ""
        data = $.parseJSON(data)
       $.each(data, function(index, month_data){
            disabled = ''
            title = ''
            month_name = month_data['month_name']
            d1_weekday_name = month_data['d1_weekday_name']
            d1_year_day = month_data['d1_year_day']
            size = month_data['size']
            is_active = month_data['is_active']
            year = month_data['year']
            if(is_active == false || exclude_months.includes(month_name) || exclude_months_local.includes(month_name) ){
                disabled = 'disabled'
                title = ' (Closed)'
            }
            month_options = month_options + "<option title='"+title + "' value='"+month_name+"' "+ disabled + " d1_weekday_name='" + d1_weekday_name + "' d1_year_day='"+ d1_year_day + "' size=" + size + " year="+year    +">"+ month_name +" " +year + " " + title +"</option>"
        })
       $("#month_list").html(month_options)
       $("[for=month_list]").show('slow').focus()
    })

}




function populate_days(month_name, calendar_mode){
        var calendar_mode = $("[name=calendar_mode]:checked").val();
        var month_element = $("#month_list").find(':selected');
        var month_name = month_element.val()
        var d1_weekday_name = month_element.attr('d1_weekday_name')
        var d1_year_day = parseInt(month_element.attr('d1_year_day'))
        var size = parseInt(month_element.attr('size'))
        var year = parseInt(month_element.attr('year'))

       // disable the non service weekdays and holidays
        var today_year_day = parseInt($("#today_year_day").text());
        var service_data = $.parseJSON($("#service-data").text())
        var exclude_holidays = service_data['holidays']
        var exclude_weekdays = service_data['exclude_weekdays']
        var exclude_months = service_data['exclude_months']
        var exclude_months_local = service_data['exclude_months_local']
        var exclude_year_days = service_data['exclude_year_days']
        weekdays = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"]

        calendar_get_method = "calendar_get"
        if(calendar_mode.includes("ethiopian")){
            calendar_get_method = "calendar_get_ethiopia"
            weekdays = ["ሰኞ", "ማክሰኞ", "እሮብ", "ሃሙስ", "ዓርብ", "ቅዳሜ", "እሁድ"]
        }
        // get days of given month
        index_of_d1_weekday_name = weekdays.indexOf(d1_weekday_name)
        adjusted_weekdays = weekdays.slice(index_of_d1_weekday_name, 7).concat(weekdays.slice(0, index_of_d1_weekday_name))
        day_html = ""

        // if month selected is same to today's month then start show customer to book from today to end of the month
        // Do not show days from day 1 up to today so that customer will not get the chance choose them for booking
        month_start_count = 1
        if(d1_year_day < today_year_day && today_year_day < (d1_year_day + size)){
            month_start_count = today_year_day - d1_year_day + 1
         }

        for(month_day=month_start_count; month_day<=size; month_day++){
            // disable holidays
            title = ''
            disabled = ''
            year_day = d1_year_day + month_day -1
            if(year_day > 365){
                year_day = year_day - 365
            }
            weekday_name = adjusted_weekdays[month_day%7]
            month_name_day = month_name + " " + month_day

            // if the day is less than today year day and if is in current month then day should be disabled
           if(exclude_months.includes(month_name)){
                title = month_name + " is closed (from Western calendar)"
                disabled = 'disabled'
            }else if(exclude_months_local.includes(month_name)){
                title = month_name + " is closed (from Ethiopian calendar)"
                disabled = 'disabled'
            }else if(exclude_weekdays.includes(weekday_name)){
                title = 'Closed on ' + weekday_name
                disabled ='disabled'
            }else if(exclude_year_days.includes(year_day)){
                title = 'Closed on this day'
                disabled ='disabled'
            }


            day_html = day_html + "<option value=" +  month_day  +" year_day="+year_day+" month_name=" + month_name +" weekday_name=" + weekday_name  +  " year=" + year  + " title='"+ title+ "' " + disabled + "> " + month_day + " -- " + weekday_name +"</option>"

        }
        $("#day_list").html(day_html)


}


function populate_time_organizations(data){
    calendar_mode = $("[name=calendar_mode]:checked").val();
    view_available_text = 'ክፍት ቦታ'
    if(calendar_mode == 'western'){ view_available_text = "left" }

    var service_data = $.parseJSON($("#service-data").text())
    var local_time = $.parseJSON($("#local-time-data").text())
    capacities = service_data['capacity']
    // for organizations populate in table format
    time_html = ""
    $.each(data, function(time, booked_list){
        capacity = capacities[time]
        // show only if oragnaization ahs capacity on that time slot
        if(capacity > 0){
            available = capacity - booked_list.length
            phoneLinks =""

            $.each(booked_list, function(key, applicant){
                a = String(applicant).split(",")
                fullname = String(a[0])
                phone = String(a[1])
                pincode = String(a[2])
                phoneLinks = phoneLinks + "<button onclick=\"show_appointment_status_modal('"+fullname+"','"+phone+"','"+ pincode+"')\">"+applicant+"</button>"
            })
            view_time = local_time[time]
            if(calendar_mode == 'western'){
                view_time = time.replace("t", "").replace('_am', ' am').replace('_pm', ' pm').replace('_', ':')
            }
            time_html = time_html + "<tr><td name='"+time +"' value=" + time + ">" + view_time + "</td><td>"+ capacity + "</td><td>"+available + "</td><td>"+phoneLinks+"</td></tr>"
        }
    })

   $("#time_list > tbody").html(time_html)
   $("[for=time_list]").show('slow').focus()

}




function populate_time_customers(data){
       $("#time_list > option").prop('selected', false).prop('disabled',true).prop('hidden', true)
        calendar_mode = $("[name=calendar_mode]:checked").val();
        local_times = $.parseJSON($("#local-time-data").text())
        service_data = $.parseJSON($("#service-data").text())
        local_times = $.parseJSON($("#local-time-data").text())
        capacities = service_data['capacity']
        time_html = ""
         // customer time in list format
        var time_keys_ordered = []
        for(i =1; i<=12; i++){
             two_digit_form = "0" + i
             if(i>=10){ two_digit_form = i }
             time_keys_ordered.push("t"+two_digit_form + "_00_am")
             time_keys_ordered.push("t"+two_digit_form + "_30_am")
        }
        for(i =1; i<=12; i++){
             two_digit_form = "0" + i
             if(i>=10){ two_digit_form = i }
             time_keys_ordered.push("t"+two_digit_form + "_00_pm")
             time_keys_ordered.push("t"+two_digit_form + "_30_pm")
        }

        $.each( time_keys_ordered, function(index, time_key ) {
            local_time = local_times[time_key]

            capacity = capacities[time_key]
            local_time = local_times[time_key]
            time = time_key.replace('t', '').replace("_am", " am").replace("_pm", " pm").replace("_", ":")
            booked_list = data[time_key]
            available = capacity - booked_list.length
            if(capacity <= 0){return true}
            disabled = 'disabled'
            if(available > 0){ disabled=''}
            visible_text = time  + " -- available (" +available + ")"
            if(calendar_mode != 'western'){
                visible_text = local_time  + " -- ቀሪ ቦታ (" +available + ")"
            }
            time_html = time_html + "<option value=" + time_key + " local_time='" +local_time + "' time='"+time +"' " + disabled + ">"+ visible_text+ "</option>"
        });
    $("#time_list").html(time_html).focus()
    $("[for=time_list]").show('slow').focus()
}
