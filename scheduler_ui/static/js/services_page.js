


$('#add_new_requirements').on("click", function(){
    value = prompt("Type new requirement name")
    validate_value = value.replace(/\s+/g, '')
    if(validate_value.length>0 & validate_value !='null'){
         count=0
         $('input[name^="requirements"]').each(function(i){
            count = i + 1
           $(this).attr("name",'requirements'+i);
        });
       element =  "<label><span class='remove_parent'  title='delete'>x</span><input type='text' name='requirements' value='" +value+ "' hidden>"+value+"</label>"
      $( "#requirements_div" ).append(element );
      }
  return false
});


$('#add_new_holidays').on("click", function(){
    value = prompt("Add new holidays, follow the standard below\n   10/1 ==> stands September 10 \n    10-15/12 ==> stands from August 10 to August 15.\n  be remind about month translation for this case\n    1 is September, ..., 12 is August ")
    value = value.replace(/\s+/g, '')
    const re1 = /^(0?[1-9]|[12][0-9]|3[01])\/(0?[1-9]|1[0123])$/; // day time dd/mm
    const re2 = /^(0?[1-9]|[12][0-9]|3[01])\-(0?[1-9]|[12][0-9]|3[01])\/(0?[1-9]|1[0123])$/; // day time dd/mm

    if (re1.test(value) || re2.test(value)){
        splitted = value.split("/")
        months_list ={1:"September", 2:"October", 3: 'November', 4:'December', 5:'January', 6:'February', 7:'March', 8:'April', 9:'May', 10:'Jun', 11:'July', 12:'August'}
        value = months_list[parseInt(splitted[1])] + " " + splitted[0]

        count=0
         $('input[name^="holidays"]').each(function(i){
            count = i+ 1
           $(this).attr("name",'holidays'+i);
        });
       element =  "<label><span class='remove_parent'   title='delete'>x</span><input type='text' name='holidays' value='"+value+"' hidden>"+value+"</label>"
      $( "#holidays_div" ).append(element );
    }else{
        alert("Invalid input '"+value+ "' should day/month ==> dd/mm or dd-dd/mm")
    }
});


$(document).delegate('.remove_parent', 'click', function(){
      if(confirm("are you sure you want to delete? '" + $(this).parent().text() + "'")){
       this.parentNode.remove();
    }
});


$( window ).ready(function() {
   // rename  holidays if same name available
    $('input[name^="holidays"]').each(function(i){
       $(this).attr("name",'holidays'+i);
    });

   // rename  requirements if same name available
  $('input[name^="requirements"]').each(function(i){
       $(this).attr("name",'requirements'+i);
    });
});

