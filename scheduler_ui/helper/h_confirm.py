import json
import unicodedata

import requests
from scheduler_ui.helper.constants import api_base_uri


def api_get_confirm(filter={}):
    uri = f"{api_base_uri}/confirm/through_email"
    resp = requests.get(uri, params=filter)
    body = unicodedata.normalize('NFKD', resp.text).encode('ascii', 'ignore')
    body = json.loads(body)
    body['status_code'] = resp.status_code
    return body


def api_post_confirm_through_email(data):
    uri = f"{api_base_uri}/confirm/through_email"
    resp = requests.post(uri, json=data)
    body = unicodedata.normalize('NFKD', resp.text).encode('ascii', 'ignore')
    body = json.loads(body)
    body['status_code'] = resp.status_code
    return body


def api_post_confirm_through_phone(data):
    uri = f"{api_base_uri}/confirm/through_phone"
    resp = requests.post(uri, json=data)
    body = unicodedata.normalize('NFKD', resp.text).encode('ascii', 'ignore')
    body = json.loads(body)
    body['status_code'] = resp.status_code
    return body