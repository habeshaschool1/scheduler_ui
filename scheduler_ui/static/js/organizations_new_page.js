

function email_new_pincode(){
    email = $("#email_new").val()
    if (validate_email(email)){
         $.post( "/confirm/through_email",{email:email}, function( data ) {
            $("#email").html(email)
            $("[for=email_new]").hide('slow')
            $("#confirm_email_sent_message").hide('hide').html("PIN-CODE sent successfully to "+ email).fadeIn(3000, "linear")
            $("[for=confirm_pincode]").delay(3000).fadeIn(3000, "linear")
        });
    }else{
            $("#email_new").css("border", "2px solid red").fadeIn(2000, "linear");
         }
}


function confirm_pincode_to_email(){
    email = $("#email_new").val()
    confirm_pincode = $("#confirm_pincode").val()
    pincode_length = confirm_pincode.replace(/ /g, "").length
    if (pincode_length > 4){
         $.get( "/confirm/through_email", {pincode:confirm_pincode, email:email}, function( data ) {
            data_email = data['result']['email']
            if(typeof data_email == 'undefined'){
             $("#confirm_pincode_fail_message").hide('slow').html("PIN_CODE not matching your email "+ email  + "<br>Change the pin and try again or <a href='/organizations_new'>click here to get new PIN_CODE</a>").show('slow')
            }else{
                $("[for=confirm_pincode]").hide('slow')
                $("#pincode").val(data['result']['pincode'])

                $("#confirm_email_sent_message").hide('slow').html("PIN-CODE Confirmed!").fadeIn(3000, 'linear')
                $("#form").delay(3000).fadeIn(3000, "linear");
            }
        });
    }else{
            $("#confirm_pincode").css("border", "2px solid red");
         }
}

$("#category_id").on('change', function(){
    if(this.value != -1){
     return false
    }

    pincode = $("#pincode").val()
    if(pincode.length < 4){
        alert("PIN-CODE is required")
        return false
    }
    name = prompt("Enter a new organization type you want to add")
    validate_name = name.replace(/\s+/g, '')
    if(validate_name.length>0 & validate_name !='null'){
         $.post( "/categories_post",{name:name, pincode:pincode}, function( data ) {
          option = data['result']
            $("#category_id").val([]);
             new_option = "<option value='" + option['category_id'] + "' selected>"+option['name'] + "</option>"
            $("#category_id").append(new_option)
         //location.reload()
        });
    }
})


$("#district_id").on('change', function(){
    if(this.value != -1){
     return false
    }

    pincode = $("#pincode").val()
    if(pincode.length < 4){
        alert("PIN-CODE is required")
        return false
    }

    name = prompt("Enter a new city name you want to add")
    validate_name = name.replace(/\s+/g, '')
    if(validate_name.length>0 & validate_name !='null'){
         $.post( "/districts_post",{city:name, pincode:pincode}, function( data ) {
            option = data['result']
            $("#district_id").val([]);
             new_option = "<option value='" + option['district_id'] + "' selected>"+option['city'] + "</option>"
            $("#district_id").append(new_option)
            //location.reload()
        });
    }
})

$("form").submit(function(){
        error = 0
    password = $('#password').val()
    password2 = $('#confirm_password').val()
    if(password != password2){
      alert("password is not matching")
      return false
    }
});


$( document ).ready(function() {
    $("[for=confirm_pincode]").hide()
    $("#form").hide()
});
