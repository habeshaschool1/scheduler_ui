import json
import unicodedata

import requests
from scheduler_ui.helper.constants import api_base_uri


def api_get_service(service_id, bearer=''):
    uri = f"{api_base_uri}/services/{service_id}"
    headers = {'Authorization': f"Bearer {bearer}"}
    resp = requests.get(uri, headers=headers)
    body = unicodedata.normalize('NFKD', resp.text).encode('ascii', 'ignore')
    body = json.loads(body)
    body['status_code'] = resp.status_code
    return body


def api_get_services(filter, bearer=''):
    uri = f"{api_base_uri}/services"
    headers = {'Authorization': f"Bearer {bearer}"}
    resp = requests.get(uri, params=filter, headers=headers)
    body = unicodedata.normalize('NFKD', resp.text).encode('ascii', 'ignore')
    body = json.loads(body)
    body['status_code'] = resp.status_code
    return body


def api_post_services(data, bearer=''):
    uri = f"{api_base_uri}/services"
    headers = {'Authorization': f"Bearer {bearer}"}
    resp = requests.post(uri, json=data, headers=headers)
    body = unicodedata.normalize('NFKD', resp.text).encode('ascii', 'ignore')
    body = json.loads(body)
    body['status_code'] = resp.status_code
    return body


def api_put_service(service_id, data, bearer=''):
    uri = f"{api_base_uri}/services/{service_id}"
    headers = {'Authorization': f"Bearer {bearer}"}
    resp = requests.put(uri, json=data, headers=headers)
    body = unicodedata.normalize('NFKD', resp.text).encode('ascii', 'ignore')
    body = json.loads(body)
    body['status_code'] = resp.status_code
    return body


def api_delete_service(service_id, bearer=''):
    uri = f"{api_base_uri}/services/{service_id}"
    headers = {'Authorization': f"Bearer {bearer}"}
    resp = requests.delete(uri, headers=headers)
    #body = unicodedata.normalize('NFKD', resp.text).encode('ascii', 'ignore')
    body ={"status_code": resp.status_code}
    return body
