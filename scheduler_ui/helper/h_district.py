import json
import unicodedata

import requests
from scheduler_ui.helper.constants import api_base_uri


def api_get_districts(district_id=None, filter={}, bearer=''):
    uri = f"{api_base_uri}/districts/"
    headers = {'Authorization': f"Bearer {bearer}"}
    if district_id:
        filter['district_id'] = int(district_id)

    resp = requests.get(uri, params=filter, headers=headers)
    body = unicodedata.normalize('NFKD', resp.text).encode('ascii', 'ignore')
    body = json.loads(body)
    body['status_code'] = resp.status_code
    return body



def api_post_districts(data, bearer=''):
    uri = f"{api_base_uri}/districts"
    headers = {'Authorization': f"Bearer {bearer}"}
    resp = requests.post(uri, json=data, headers=headers)
    body = unicodedata.normalize('NFKD', resp.text).encode('ascii', 'ignore')
    body = json.loads(body)
    body['status_code'] = resp.status_code
    return body


def api_put_districts(district_id, data, bearer=''):
    uri = f"{api_base_uri}/districts/{district_id}"
    headers = {'Authorization': f"Bearer {bearer}"}
    resp = requests.put(uri, json=data, headers=headers)
    body = unicodedata.normalize('NFKD', resp.text).encode('ascii', 'ignore')
    body = json.loads(body)
    body['status_code'] = resp.status_code
    return body


def api_delete_districts(district_id, bearer=''):
    uri = f"{api_base_uri}/districts/{district_id}"
    headers = {'Authorization': f"Bearer {bearer}"}
    resp = requests.delete(uri, headers=headers)
    # body = unicodedata.normalize('NFKD', resp.text).encode('ascii', 'ignore')
    body = {}  #json.loads(body)
    body['status_code'] = resp.status_code
    return body