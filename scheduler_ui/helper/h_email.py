import json
import unicodedata

import requests
from scheduler_ui.helper.constants import api_base_uri


def api_post_email(data, bearer=''):
    uri = f"{api_base_uri}/emails"
    headers = {'Authorization': f"Bearer {bearer}"}
    resp = requests.post(uri, json=data, headers=headers)
    body = unicodedata.normalize('NFKD', resp.text).encode('ascii', 'ignore')
    body = json.loads(body)
    body['status_code'] = resp.status_code
    return body
