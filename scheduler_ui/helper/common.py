from urllib.parse import urlparse

from flask import flash

from scheduler_ui.helper.constants import ALERT_ERROR, ALERT_SUCCESS


def change_filter_to_dic(filter_str=""):
    """ this method will get a string with query filter format of key=value&&key=value
    and return a json key value form"""

    if "?" in filter_str:
        filter_str = filter_str.split("?")[1]

    filter_json = {}
    if "&" in filter_str:
        pairs = filter_str.split("&")
        for pair in pairs:
            if "=" in pair:
                key, value = pair.split("=")
                filter_json[key] = value
    return filter_json


def get_url_info(url_data, key):
    url_data = urlparse(url_data)

    if key == 'url':
        hostname = url_data.hostname
        return hostname
    elif key == 'port':
        port = url_data.port or (443 if url_data.scheme == 'https' else 80)
        return port
    elif key == 'query':
        query = url_data.query
        return query
    elif key == 'netloc':
        netloc = url_data.netloc
        return netloc
    elif key == 'path':
        path = url_data.path
        return path
    else:
        return "no key"


def flash_success(msg_txt, category=ALERT_SUCCESS):
    """ this method will help to have customized flash messages"""
    msg_txt = msg_txt.lower()
    if "segment" in msg_txt or "token has expired" in msg_txt:
        msg_txt = "login required"
        # TODO logout user here
    flash(msg_txt, category)


def flash_alert(msg_txt, category=ALERT_ERROR):
    """ this method will help to have customized flash messages"""
    if "segment" in msg_txt or "token has expired" in msg_txt:
        msg_txt = "login required"
        # TODO logout user here
    flash(msg_txt, category)


def list_to_str(data_list, join_str="*"):
    str_data = None
    for data in data_list:
        if str_data is None:
            str_data = str(data)
        else:
            str_data = f"{str_data}{join_str} {str(data)}"
    return str_data


def str_to_int_list(str_data, join_str=","):
    data = str_data.split(join_str)
    int_list = []
    for d in data:
        d = int(d)
        int_list.append(d)

    return int_list


def get_list(data, field, data_type=str):
    """
    filter keys which begin by the given field
    get the values
    and prepare a list and return
    """
    # to increase capacity I did write  the for loop two times
    my_list = []
    if data_type is int:
        for key, value in data.items():
            if key.startswith(field):
                if value not in my_list:
                    my_list.append(int(value))
    else:
        for key, value in data.items():
            if key.startswith(field):
                if value not in my_list:
                    my_list.append(value)
    return list(set(my_list))


def change_et_to_greg(y, m, d, h, min):
    y = int(y) + 7
    m = int(m)
    d = int(d)
    h = int(h)

    month_days ={
        1:31,
        2:28,
        3:31,
        4:30,
        5:31,
        6:30,
        7:31,
        8:31,
        9:30,
        10:31,
        11:30,
        12:31
    }

    month_et_greg = {
        1:9,
        2:10,
        3:11,
        4:12,
        5:1,
        6:2,
        7:3,
        8:4,
        9:5,
        10:6,
        11:7,
        12:8,
        13:9
    }
    # time converter
    h = h + 7
    if h > 24:
        h = h - 24
        d = d + 1

    # day converter
    d = d + 8
    m = month_et_greg[m]

    d_size = month_days[m]
    if d > d_size:
        d = d - d_size
        m = m + 1
        # increase year if moved to January
        if m == 1:
            y = y + 1

    y = str(y)
    m = str(m)
    d = str(d)
    h = str(h)

    # change m, d and t format to 2 digit form
    if len(m) == 1:
        m = f"0{m}"
    if len(d) == 1:
        d = f"0{d}"
    if len(h) == 1:
        h = f"0{h}"

    date = {'y':y, 'm':m, 'd':d, "h":h, "min":min}

    return date


def make_google_event_link(y, m, d, h, min, phone, pincode, service, organization):
    date = change_et_to_greg(y, m, d, h, min)
    full_greg_date = f"{date['y']}{date['m']}{date['d']}T{date['h']}{date['min']}4000Z"
    time_range = full_greg_date + "/" + full_greg_date
    details = f"You have appointment today for {service} on {organization} organization, to see the detail open http://192.168.1.3/appointments_status_page?phone={phone}M%26pincode={pincode}"
    href = f"https://www.google.com/calendar/render?action=TEMPLATE&text={service} at {organization}&dates={time_range}&details={details}"  # + &location=Waldorf+Astoria,+301+Park+Ave+,+New+York,+NY+10022&sf=true&output=xml"
    return href.replace(' ', '+')
