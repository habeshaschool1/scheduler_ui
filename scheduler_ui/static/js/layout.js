
(function ($) {
	  $.each(['show', 'hide'], function (i, ev) {
	    var el = $.fn[ev];
	    $.fn[ev] = function () {
	      this.trigger(ev);
	      return el.apply(this, arguments);
	    };
	  });
	})(jQuery);


$('span.close').on("click", function(){
  $("#modal").hide('slow');
});

function find_object_from_array(data, key, value) {
    // will filter a specific object
    var selected = {}
    $.each(data, function(i, item) {
        if(item[key] == value){
            selected =  item
            return
        }
    });
    return selected
}

function translate_month(month_number){
    months = {1:'Sep-መስከረም', 2:'Oct-ጥቅምት', 3:'Nov-ህዳር', 4:'Des-ታህሳስ', 5:'Jan-ጥር', 6:'Feb-የካቲት', 7:'Mar-መጋቢት', 8:'Apr-ሚያዚያ', 9:'May-ግንቦት', 10:'Jun-ሰኔ', 11:'Jul-ሃምሌ', 12:'Aug-ነሃሴ', 13:'ጳግሜ'}
    return months[month_number]
}


function translate_weekday(weekday_number){
        days = {1:'Ma-ሰኞ', 2:'Tu-ማክሰኞ', 3:'We-ረቡዕ', 4:'Th-ሓሙስ', 5:'Fr-ዓርብ', 6:'Sa-ቅዳሜ', 7:'Su-እሁድ'}
        return days[weekday_number]
}

function translate_time(selected_time){
    clock = ' am-ቀን'
    if(selected_time >= 24){
        clock = ' pm-ማታ'
    }
    h = 0
    m = "00"
    if(selected_time%2 == 0){
        h = selected_time/2
    }else{
        h = (selected_time - 1)/2
        m = "30"
    }
    if (h == 0){
        h = 12
    }
    h = String(h)
    if(h.length == 1){
        h = "0"+ h
    }
    return h + ":" + m + clock
}

function validate_email(email) {
  const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(email);
}


$().ready(function(){ $("body").css({visibility:'visible'})});
$("#banner").hide().fadeIn(2000).delay(5000).fadeOut(5000);