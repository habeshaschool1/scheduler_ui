
function update_appointments_status(phone, org_id, m, d){
       status = "Cancelled"
       reason = "cancelled by customer";
       if (confirm('Are you sure you want to cancel this appointment?')) {
             $.post( "/organizations_appointment_status",{phone:phone, organization_id:org_id, m: m, status:status, reason:reason}, function( data ) {
                data = data['result']
                location.reload()
            });
       }
}

function email_appointment_status(phone, pincode, service_id){
    email = prompt("Enter email to send pincode");
    if (validate_email(email)){
         $.post( "/appointments_status_email",{email:email, phone:phone, pincode:pincode, service_id:service_id}, function( data ) {
            alert(data['msg'])
        });
    }else{
        alert("Invalid email, " + email)
    }
}
