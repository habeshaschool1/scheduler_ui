api_base_uri = "http://127.0.0.1:5000"
ALERT_ERROR = "error"
ALERT_INFO = "info"
ALERT_SUCCESS = "success"

LOCAL_MONTH_DAYS_DIFF = (6, 7, 8, 9, 10, 11, 12)

WEEKDAYS = {
    "Monday": "ሰኞ",
    "Tuesday": "ማክሰኞ",
    "Wednesday": "እሮብ",
    "Thursday": "ሃሙስ",
    "Friday": "ዓርብ",
    "Saturday": "ቅዳሜ",
    "Sunday": "እሁድ",
}

MONTHS = {
    "September": {'number': 9, 'local_name': "መስከረም", 'previous_month': 'August', 'next_month': 'October'},
    "October": {'number': 10, 'local_name': "ጥቅምት", 'previous_month': 'September', 'next_month': 'November'},
    "November": {'number': 11, 'local_name': "ህዳር", 'previous_month': 'October', 'next_month': 'December'},
    "December": {'number': 12, 'local_name': "ታህሳስ", 'previous_month': 'November', 'next_month': 'January'},
    "January": {'number': 1, 'local_name': "ጥር", 'previous_month': 'December', 'next_month': 'February'},
    "February": {'number': 2, 'local_name': "የካቲት", 'previous_month': 'January', 'next_month': 'March'},
    "March": {'number': 3, 'local_name': "መጋቢት", 'previous_month': 'February', 'next_month': 'April'},
    "April": {'number': 4, 'local_name': "ሚያዚያ", 'previous_month': 'March', 'next_month': 'May'},
    "May": {'number': 5, 'local_name': "ግንቦት", 'previous_month': 'April', 'next_month': 'Jun'},
    "Jun": {'number': 6, 'local_name': "ሰኔ", 'previous_month': 'May', 'next_month': 'July'},
    "July": {'number': 7, 'local_name': "ሃምሌ", 'previous_month': 'Jun', 'next_month': 'August'},
    "August": {'number': 8, 'local_name': "ነሃሴ", 'previous_month': 'July', 'next_month': 'September'}

}

MONTHS_LOCAL = {'መስከረም':{}, 'ጥቅምት':{}, 'ህዳር':{},
                'ታህሳስ':{}, 'ጥር':{}, 'የካቲት':{},
                'መጋቢት':{}, 'ሚያዚያ':{}, 'ግንቦት':{},
                'ሰኔ':{}, 'ሃምሌ':{}, 'ነሃሴ':{}, 'ጷጉሜ': {}
                }

LOCAL_TIME = {
    't08_00_am': 'ጥዋት 02:00',
    't08_30_am': 'ጥዋት 02:30',
    't09_00_am': 'ጥዋት 03:00',
    't09_30_am': 'ጥዋት 03:30',
    't10_00_am': 'ቀን 04:00',
    't10_30_am': 'ቀን 04:30',
    't11_00_am': 'ቀን 05:00',
    't11_30_am': 'ቀን 05:30',
    't12_00_am': 'ቀን 06:00',
    't12_30_am': 'ቀን 06:30',
    't01_00_pm': 'ቀን 07:00',
    't01_30_pm': 'ቀን 07:30',
    't02_00_pm': 'ቀን 08:00',
    't02_30_pm': 'ቀን 08:30',
    't03_00_pm': 'ቀን 09:00',
    't03_30_pm': 'ቀን 09:30',
    't04_00_pm': 'ቀን 10:00',
    't04_30_pm': 'ቀን 10:30',
    't05_00_pm': 'ቀን 11:00',
    't05_30_pm': 'ቀን 11:30',
    't06_00_pm': 'ምሽት 12:00',
    't06_30_pm': 'ምሽት 12:30',
    't07_00_pm': 'ምሽት 01:00',
    't07_30_pm': 'ምሽት 01:30',
    't08_00_pm': 'ምሽት 02:00',
    't08_30_pm': 'ምሽት 02:30',
    't09_00_pm': 'ምሽት 03:00',
    't09_30_pm': 'ምሽት 03:30',
    't10_00_pm': 'ምሽት 04:00',
    't10_30_pm': 'ምሽት 04:30',
    't11_00_pm': 'ምሽት 05:00',
    't11_30_pm': 'ምሽት 05:30',
    't12_00_pm': 'ለሊት 06:00',
    't12_30_pm': 'ለሊት 06:30',
    't01_00_am': 'ለሊት 07:00',
    't01_30_am': 'ለሊት 07:30',
    't02_00_am': 'ለሊት 08:00',
    't02_30_am': 'ለሊት 08:30',
    't03_00_am': 'ለሊት 09:00',
    't03_30_am': 'ለሊት 09:30',
    't04_00_am': 'ንጋት 10:00',
    't04_30_am': 'ንጋት 10:30',
    't05_00_am': 'ንጋት 11:00',
    't05_30_am': 'ንጋት 11:30',
    't06_00_am': 'ጥዋት 12:00',
    't06_30_am': 'ጥዋት 12:30',
    't07_00_am': 'ጥዋት 01:00',
    't07_30_am': 'ጥዋት 01:30'

}

USER_SYSTEM = 'system'
USER_ADMIN = 'admin'
USER_USER = 'user'
