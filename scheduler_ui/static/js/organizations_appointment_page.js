
function show_appointment_status_modal(fullname, phone, pincode){
    service_id = $('#service_list  option:selected').val()
    m = $('#month_list  option:selected').val()
    d = $('#day_list  option:selected').val()

    $('#modal_body').html("<input type='text' value='inprogress'>")
    fullname_element = "<span>Full Name: "+fullname+"</span><br>"
    phone_element = "<span>Phone: "+phone+"</span><br>"
    pincode_element = "<span>PIN-CODE: "+pincode+"</span><br>"
    service_name_element = "<span>Service Name: "+$('#service_list option:selected').text()+"</span><br>"
    month_element = "<span>Month: "+$('#month_list option:selected').text()+ "</span><br>"
    day_element = "<span>Day: "+$('#day_list option:selected').text()+"</span><br>"
    processed_element="<span class='add' id='change_status' placeholder='to show that you have processed this appointment successfully' onclick=\"change_appointments_status('"+service_id+"','"+m+"','"+d+"','"+ pincode+ "','Processed')\">Close this Appointment</span>"
    cancel_element="<span class='add id='change_status' placeholder='this is to remove this appointment from this appointment date and time, so that the time slot will be free for others' onclick=\"change_appointments_status('"+service_id+"','"+m+"','"+d+"','"+ pincode+ "','Cancelled')\">Canceled this Appointment</span>"
    $('#modal_body').html("<hr>"+service_name_element + month_element + day_element + fullname_element +phone_element +pincode_element +"<hr>" + processed_element + cancel_element)
    $('#modal').show()
   }


function change_appointments_status(service_id, m, d, pincode, status){
    post_object = {
            'service_id':service_id,
            'm':m,
            'd': d,
            'pincode': pincode,
            'status' : status,
            'reason': "Changed by Employee",
        }
    reason = "changed by employer";
         $.post( "/organizations_appointment_status",post_object, function( data ) {
            data = data['result']
            $('#modal_body').html("<span> Appointment has been successfully <b>"+ status + "</span>")
         });
}


function search_appointment_by_phone(){
    search_obj ={'phone': $('#phone').val()};
    phone_search_html = ""
   $.post( "/appointments_status_get",search_obj, function( data ) {
   if(data['result'].length == 0){
     phone_search_html = "no appointment for this phone"
   }else{
        $.each(data['result'], function(key, d){
            record = d['service_name'] + " &#8594; "+ d['fullname'] +", " + d['pincode'] +" &#8594; " +d['complete_date'] + " &#8594; " + d['status']
            if(d['reason']){
                record = record + ' &#8594; ' +d['reason']
            }
            phone_search_html =phone_search_html +  "<span>" + record+ "</span><hr>"
        })
    }
    $('#search_by_phone_result').html(phone_search_html + "<a href='#' onclick='clear_phone_search()'>clear this phone search</a>")
    });
}

function clear_phone_search(){
    $('#search_by_phone_result').html('')
    $('#phone').val('')
}

$( window ).ready(function() {
  $("[for=customer_info]").hide()
    $("[for=time_list]").hide()
    $("[for=day_list]").hide()
    $("[for=month_list]").hide()
    $("[for=service_info]").hide()
    $("[for=organization_info]").show('slow')
});
